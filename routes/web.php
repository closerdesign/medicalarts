<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'PagesController@homepage')->name('home');
Route::get('packages', 'PagesController@packages')->name('packages');
Route::get('gallery', 'PagesController@gallery')->name('gallery');
Route::get('blog', 'PagesController@blog')->name('blog');
Route::get('post/{id}', 'PagesController@post')->name('post');
Route::get('contact', 'PagesController@contact')->name('contact');

Route::get('lang/{id}', 'PagesController@language');

Route::post('contact', 'FormsController@contact');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
