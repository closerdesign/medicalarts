<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class FormsController extends Controller
{
    /**
     *  Contact
     *
     *
     */

    public function contact(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name'  => 'required',
            'phone'      => 'required',
            'country_name'    => 'required',
            'email'      => 'required|email',
        ]);

        try
        {
            $client = new Client();

            $response = $client->post('https://closerdesign.net/api/lead/74',
                [
                'form_params' => $request->toArray()
                ]
            );

            Session::flash('message', [
                'type'  => 'success',
                'message' => Lang::get('forms.thank-you-msg')
            ]);

            return back();
        }

        catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }
}
