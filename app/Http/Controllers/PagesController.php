<?php

namespace App\Http\Controllers;

use Artesaos\SEOTools\Facades\SEOMeta;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class PagesController extends Controller
{
    /**
     *  Homepage
     *
     *
     */

    public function homepage()
    {
        try
        {
            $client = new Client();

            $response = $client->get('https://closerdesign.net/api/portfolios/74');

            $testimonials = json_decode( $response->getBody() );
        }

        catch ( \Exception $e )
        {
            return $e->getMessage();
        }

        try
        {
            $client = new Client();

            $response = $client->get('https://closerdesign.net/api/gallery/51');

            $gallery = json_decode( $response->getBody() );
        }

        catch ( \Exception $e )
        {
            return $e->getMessage();
        }


        SEOMeta::setTitle(Lang::get('general.home'));
        SEOMeta::setDescription(Lang::get('general.description'));


        return view('pages.homepage', compact('testimonials','gallery'));
    }

    /**
     *  Packages
     *
     *
     */

    public function packages()
    {
        try
        {
            $client = new Client();

            $response = $client->get('https://closerdesign.net/api/product-category/282');

            $packages = json_decode( $response->getBody() );

            $response = $client->get('https://closerdesign.net/api/product-category/283');

            $procedures = json_decode( $response->getBody() );

            $response = $client->get('https://closerdesign.net/api/product-category/284');

            $additional_procedures = json_decode( $response->getBody() );

            $response = $client->get('https://closerdesign.net/api/product-category/285');

            $other_procedures = json_decode( $response->getBody() );


        }

        catch ( \Exception $e )
        {
            return $e->getMessage();
        }

        SEOMeta::setTitle(Lang::get('general.packages'));
        SEOMeta::setDescription(Lang::get('general.packages-intro'));

        return view('pages.packages', compact('packages', 'procedures', 'additional_procedures', 'other_procedures'));
    }


    /**
     *  Gallery
     *
     *
     */

    public function gallery()
    {
        try
        {
            $client = new Client();

            $response = $client->get('https://closerdesign.net/api/gallery/51');

            $images = json_decode($response->getBody());

            $images = $images->images;

            SEOMeta::setTitle( Lang::get('gallery.title') );

            return view('pages.gallery', compact('images'));

        }catch (\Exception $e)
        {
            return $e->getMessage();
        }

    }

    /**
     *  Blog
     *
     *
     */

    public function blog()
    {
        try
        {
            $client = new Client();

            $response = $client->get('https://closerdesign.net/api/posts/74');

            $blog = json_decode( $response->getBody());

            SEOMeta::setTitle(Lang::get('general.blog'));
            SEOMeta::setDescription(Lang::get('general.blog-description'));

            return view('pages.blog', compact('blog'));

        }

        catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    /**
     * Posts
     *
     *
     */

    public function post($id)
    {
        $id = explode('-', $id)[0];

        try
        {
            $client = new Client();

            $response = $client->get('https://closerdesign.net/api/post/' . $id);

            $post = json_decode( $response->getBody());

            SEOMeta::setTitle($post->title);
            SEOMeta::setDescription(strip_tags($post->content));

            return view('pages.post', compact('post'));

        }

        catch(\Exception $e)
        {
            return $e->getMessage();
        }
    }

    /**
     *  Contact
     *
     *
     */

    public function contact()
    {
        SEOMeta::setTitle(Lang::get('contact.title'));
        SEOMeta::setDescription(Lang::get('contact.intro'));

        return view('pages.contact');
    }

    /**
     *  Language
     *
     *
     */

    public function language($id)
    {
        Session::put('language', $id);

        return back();
    }
}
