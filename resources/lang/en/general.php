<?php

return [

    'title' => 'Home',
    'description' => 'Colombian Company, Leader in Medical Tourism, under the concept of Luxury, Elegance and Good Taste.',
    'assessment-request' => 'FREE Online Assessment',
    'start-free-online-assessment-now' => 'Start Your Online Assessment Now!',

    'first-name' => 'First Name',
    'last-name' => 'Last Name',
    'phone' => 'Phone',
    'country' => 'Country',
    'email' => 'Email',
    'start-now' => 'Start Now',

    'home' => 'Home',
    'packages' => 'Packages',
    'packages-intro' => 'Our packages include everything you need in order to live the most amazing experience of pleasure and beautifulness.',
    'packages-features' => '
        <ul>
            <li>Plastic Surgery With Certified Specialists</li>
            <li>Post Surgery Care</li>
            <li>Accomodation</li>
            <li>Transportation</li>
            <li>Nourishment</li>
            <li>24/7 Comforting</li>
            <li>Entertainment</li>
            <li>And more...</li>
        </ul>
    ',
    'see-packages' => 'See Packages',

    'testimonials' => 'Testimonials',
    'testimonials-intro' => 'We pour love on every case we treat, to make sure you live a great experience. Listen to those who already lived that experience before making any decision.',
    'watch-video' => 'Watch Video',

    'blog' => 'News',
    'blog-description' => 'We write some articles for you where you can find advices about beauty, health, cares, and advice that will help you when you have plastic surgery with us.',

    'contact' => 'Contact',

    'language' => 'es',

    'call-now' => 'Call Us Now!',
    'us-and-canada' => 'From US & Canada',

    'accomodation' => 'Accomodation',
    'gallery' => 'Gallery',

];