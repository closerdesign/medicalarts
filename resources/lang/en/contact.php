<?php

return [

    'title'    => 'Contact',
    'intro'    => '<p>Let us know about any questions or concerns you may have.</p>',
    'first-name' => 'Name',
    'last-name' => 'Last Name',
    'email'    => 'Email',
    'phone'    => 'Phone',
    'country'  => 'Country',
    'comments' => 'Comments',
    'send'     => 'Send',

];