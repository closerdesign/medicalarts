<?php

return [

    'hero-intro' => 'Plastic Surgery in Colombia',
    'hero-title' => 'We Think Of Your Body like of a Master Piece',
    'hero-features' => '
        <ul>
            <li>Only Certified & Specialized Surgeons</li>
            <li>All Inclusive Packages</li>
            <li>Premium Accomodation</li>
            <li>No Additional Costs</li>
            
        </ul>
    '

];