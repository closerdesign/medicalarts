<?php

return [

    'title' => 'Inicio',
    'description' => 'Empresa Colombiana, Líder en Turismo Médico, bajo el concepto de Lujo, Elegancia y Buen Gusto.',
    'assessment-request' => 'Asesoría en Línea GRATIS',
    'start-free-online-assessment-now' => 'Inicia tu Asesoría!',

    'first-name' => 'Nombre',
    'last-name' => 'Apellido',
    'phone' => 'Teléfono',
    'country' => 'País',
    'email' => 'Correo Electrónico',
    'start-now' => 'Comenzar',

    'home' => 'Inicio',
    'packages' => 'Paquetes',
    'packages-intro' => 'Nuestros paquetes incluyen todo lo que necesitas para vivir la experiencia más increíble de placer y belleza.',
    'packages-features' => '
        <ul>
            <li>Cirugía Plástica con Especialistas Certificados</li>
            <li>Cuidados Post Quirúrgicos</li>
            <li>Alojamiento</li>
            <li>Transporte</li>
            <li>Alimentación</li>
            <li>Acompañamiento las 24 Horas del Día</li>
            <li>Entretenimiento</li>
            <li>Y Mucho Más...</li>
        </ul>
    ',
    'see-packages' => 'Ver Paquetes',

    'testimonials' => 'Testimoniales',
    'testimonials-intro' => 'Ponemos todo nuestro empeño en cada caso particular para asegurarnos de que vivas una experiencia satisfactoria. Escucha a quienes han tenido la oportunidad de vivirla, antes de tomar tu desición.',
    'watch-video' => 'Ver Video',

    'blog' => 'Blog',
    'blog-description' => 'Escribimos algunos artículos para usted donde podrá encontrar tips de belleza, salud, cuidados, y consejos que le serviran al momento de realizarse una cirugía plástica con nosotros.',

    'contact' => 'Contacto',

    'language' => 'en',

    'call-now' => '¡Llama Ya!',
    'us-and-canada' => 'Desde USA & Canadá',

    'accomodation' => 'Alojamiento',
    'gallery' => 'Galería',

];