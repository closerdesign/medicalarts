<?php

return [

    'title'    => 'Contacto',
    'intro'    => '<p>Si tienes alguna pregunta o comentario, ponte en contacto con nosotros. Uno de nuestros asesores te responderá lo antes posible.</p>',
    'first-name' => 'Nombre',
    'last-name' => 'Apellido',
    'email'    => 'Correo Electrónico',
    'phone'    => 'Teléfono',
    'country'  => 'País',
    'comments' => 'Comentarios',
    'send'     => 'Enviar',

];