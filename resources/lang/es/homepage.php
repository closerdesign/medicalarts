<?php

return [

    'hero-intro' => 'Cirugía Plástica en Colombia',
    'hero-title' => 'Pensamos en tu Cuerpo como en una Obra de Arte',
    'hero-features' => '
        <ul>
            <li>Únicamente Cirujanos Especializados & Certificados</li>
            <li>Paquetes Todo Incluido</li>
            <li>Alojamiento de Primera Categoría</li>
            <li>Sin Cargos Extra o Costos Adicionales</li>
            
        </ul>
    '

];