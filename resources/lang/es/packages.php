<?php

return [

    'title'    => 'Paquetes',
    'procedures' => 'Procedimientos',
    'additional-procedures' => 'Procedimientos Adicionales',
    'other-procedures' => 'Otros Procedimientos',

];