<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TQQHLHX');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! SEO::generate() !!}

    <link rel="stylesheet" href="/bower/magnific-popup/dist/magnific-popup.css">

    <link rel="icon" type="image/png" href="/img/logo.png"/>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <script src="https://kit.fontawesome.com/8a0881e50d.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- jQuery 1.7.2+ or Zepto.js 1.0+ -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script src="/bower/magnific-popup/dist/jquery.magnific-popup.min.js"></script>

</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQQHLHX"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="app">

    @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin-bottom: 0;">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if ( Session::has('message') )
        <div class="alert alert-{{ Session::get('message')['type'] }}" style="margin-bottom: 0;">
            {{ Session::get('message')['message'] }}
        </div>
    @endif

        <div class="sticky-top">
    <nav class="navbar navbar-expand-md navbar-light bg-white">
        <div class="container">
            <a class="navbar-brand" data-toggle="modal" data-target="#ctaModal">
                @lang('general.assessment-request')
            </a>


            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="{{ route('home') }}" class="nav-link">
                            <i class="fas fa-home"></i>
                            @lang('general.home')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('packages') }}" class="nav-link">
                            <i class="fas fa-user-md"></i>
                            @lang('general.packages')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('gallery') }}" class="nav-link">
                            <i class="fas fa-images"></i>
                            @lang('general.gallery')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('blog') }}" class="nav-link">
                            <i class="fas fa-th-large"></i>
                            @lang('general.blog')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('contact') }}" class="nav-link">
                            <i class="fas fa-envelope"></i>
                            @lang('general.contact')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ action('PagesController@language', Lang::get('general.language')) }}"
                           class="nav-link text-uppercase">
                                <span class="badge badge-secondary">
                                    <i class="fas fa-globe"></i> @lang('general.language')
                                </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <header v-scroll-reveal.reset>
        <a href="{{ action('PagesController@homepage') }}" alt="Medical Arts Logo">
            <img src="/img/logo.png" alt="Medical Arts Logo">
        </a>
    </header>
        </div>


    <main>
        @yield('content')
    </main>

    <section id="call-now" v-scroll-reveal.reset>
        <div class="container">
            <h1>
                @lang('general.call-now')
            </h1>
            <h2>
                @lang('general.us-and-canada')
            </h2>
            <h3>
                <a href="tel:+18007591482">+1(800)759-1482</a>
            </h3>
        </div>
    </section>

    <footer>
        Medical Arts &copy; All Rights Reserved {{ date('Y') }}.<br/>Digital Strategy By <a
            href="https://closerdesign.net" target="_blank">Closer Design Networks</a>
        <div class="container">
            <div class="row">
                <div class="col-md-12 social-icons">
                    <a href="https://www.facebook.com/cirugiaplasticamedicalarts/" target="_blank">
                        <i class="fab fa-facebook-square fa-2x"></i>
                    </a>
                    <a href="https://www.facebook.com/cirugiaplasticamedicalarts/" target="_blank">
                        <i class="fab fa-instagram fa-2x"></i>
                    </a>
                </div>
            </div>
        </div>
        <ssl-component></ssl-component>
    </footer>

    <section id="cta">
        <div class="inner">
            <div class="container">
                <div class="row">
                    <div class="col-3 whatsapp">
                        <a target="_blank" href="https://wa.me/573505926112/?text=Quiero%20Asesoria%20Personalizada">
                            <i class="fab fa-whatsapp fa-2x"></i>
                        </a>
                    </div>
                    <div class="col-3 messenger">
                        <a target="_blank" href="https://m.me/cirugiaplasticamedicalarts">
                            <i class="fab fa-facebook-messenger fa-2x"></i>
                        </a>
                    </div>
                    <div class="col-3 phone">
                        <a target="_blank" href="tel:+18007591482">
                            <i class="fas fa-phone fa-2x"></i>
                        </a>
                    </div>
                    <div class="col-3 contact">
                        <a target="_blank" href="{{ route('contact') }}">
                            <i class="fas fa-envelope fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="ctaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="exampleModalCenterTitle">@lang('general.assessment-request')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-3">
                                <a target="_blank"
                                   href="https://wa.me/573505926112/?text=Quiero%20Asesoria%20Personalizada">
                                    <i class="fab fa-whatsapp fa-2x"></i>
                                </a>
                            </div>
                            <div class="col-3">
                                <a target="_blank" href="https://m.me/cirugiaplasticamedicalarts">
                                    <i class="fab fa-facebook-messenger fa-2x"></i>
                                </a>
                            </div>
                            <div class="col-3">
                                <a target="_blank" href="tel:+18007591482">
                                    <i class="fas fa-phone fa-2x"></i>
                                </a>
                            </div>
                            <div class="col-3">
                                <a target="_blank" href="{{ route('contact') }}">
                                    <i class="fas fa-envelope fa-2x"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>

</body>
</html>
