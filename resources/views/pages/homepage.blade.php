@extends('layouts.app')

@section('content')

    <section id="hero" v-scroll-reveal.reset>
        <div class="form">
            <h1>@lang('homepage.hero-intro')</h1>
            <h2>@lang('homepage.hero-title')</h2>
            <form action="{{ action('FormsController@contact') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="first_name">@lang('general.first-name')</label>
                    <input type="text" class="form-control text-center" name="first_name"
                           value="{{ old('first_name') }}" required>
                </div>
                <div class="form-group">
                    <label for="last_name">@lang('general.last-name')</label>
                    <input type="text" class="form-control text-center" name="last_name" value="{{ old('last_name') }}"
                           required>
                </div>
                <div class="form-group">
                    <label for="phone">@lang('general.phone')</label>
                    <input type="text" class="form-control text-center" name="phone" value="{{ old('phone') }}"
                           required>
                </div>
                <div class="form-group">
                    <label for="country">@lang('general.country')</label>
                    <input type="text" class="form-control text-center" name="country_name"
                           value="{{ old('country_name') }}" required>
                </div>
                <div class="form-group">
                    <label for="email">@lang('general.email')</label>
                    <input type="email" class="form-control text-center" name="email" value="{{ old('email') }}"
                           required>
                </div>
                <button class="btn btn-primary btn-block">
                    @lang('general.start-free-online-assessment-now')
                </button>
            </form>
        </div>
    </section>

    <section id="features" v-scroll-reveal.reset>
        @lang('homepage.hero-features')
    </section>

    <section id="accomodation" v-scroll-reveal.reset>
        <div class="container-fluid">
            <h1>@lang('general.accomodation')</h1>
            <div class="row">
                @php $i=0; @endphp
                @foreach( $gallery->images as $image )
                    @if($i < 4)
                        <div class="col-md-3">
                            <div class="item-img">
                                <a href="{{ route('gallery') }}">
                                    <img src="/img/icon-plus.png" class="img-fluid plus" alt="">
                                    <img class="img-fluid" src="{{ $image->image }}" alt="">
                                </a>
                            </div>
                        </div>
                    @endif
                    @php $i++; @endphp
                @endforeach
            </div>
        </div>
    </section>

    <section id="packages" v-scroll-reveal.reset>
        <div class="container">
            <h1>@lang('general.packages')</h1>
            <div class="row">
                <div class="col-md-6">
                    @lang('general.packages-intro')
                </div>
                <div class="col-md-6">
                    @lang('general.packages-features')
                </div>
            </div>
            <a href="{{ action('PagesController@packages') }}" class="btn btn-primary btn-block">
                @lang('general.see-packages')
            </a>
        </div>
    </section>

    {{--<section id="testimonials">--}}
    {{--<div class="container">--}}
    {{--<h1>@lang('general.testimonials')</h1>--}}
    {{--<p>@lang('general.testimonials-intro')</p>--}}
    {{--<div class="row">--}}

    {{--@php $i = 0; @endphp--}}

    {{--@foreach( $testimonials as $testimonial )--}}

    {{--@if( $i < 3 )--}}
    {{--<div class="col-md-4">--}}
    {{--<div class="testimonial">--}}
    {{--<div class="row">--}}
    {{--<div class="col-4 offset-4">--}}
    {{--<a href="">--}}
    {{--<img src="{{ $testimonial->cover }}" class="img-fluid rounded-circle" alt="">--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<p class="lead text-center">{{ $testimonial->client }}</p>--}}
    {{--{!! $testimonial->description !!}--}}
    {{--<button data-toggle="modal" data-target="#testimonial{{ $testimonial->id }}" class="btn btn-primary btn-block">--}}
    {{--@lang('general.watch-video')--}}
    {{--</button>--}}
    {{--</div>--}}

    {{--<!-- Modal -->--}}
    {{--<div class="modal fade" id="testimonial{{ $testimonial->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
    {{--<div class="modal-dialog" role="document">--}}
    {{--<div class="modal-content">--}}
    {{--<div class="modal-header">--}}
    {{--<h5 class="modal-title" id="exampleModalLabel">{{ $testimonial->client }}</h5>--}}
    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
    {{--<span aria-hidden="true">&times;</span>--}}
    {{--</button>--}}
    {{--</div>--}}
    {{--<div class="modal-body">--}}
    {{--<div class="embed-responsive embed-responsive-16by9">--}}
    {{--<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $testimonial->yt_video }}?rel=0" allowfullscreen></iframe>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="modal-footer">--}}
    {{--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--@endif--}}

    {{--@php $i++ @endphp--}}

    {{--@endforeach--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}

@endsection
