@extends('layouts.app')

@section('content')

    <section id="title" v-scroll-reveal.reset>
        <h1>{{ $post->title }}</h1>
    </section>

    <section id="content"  v-scroll-reveal.reset>
        <div class="container">

                <div class="img">
                    <img src="{{ $post->image }}" alt="{{ $post->title }}" class="img-fluid">
                </div>

                <div class="col-md-12">
                    <p>{!! $post->content !!}</p>
                </div>

        </div>
    </section>

    @endsection
