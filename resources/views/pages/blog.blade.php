@extends('layouts.app')

@section('content')

    <section id="title">
        <h1>@lang('general.blog')</h1>
    </section>

    <div id="content">
        <div id="blog-content">
            <div class="container" v-scroll-reveal.reset>
                <div class="row">
                    @foreach($blog->data as $post)
                        <div class="col-md-4 col-lg-6">
                            <div class="post">
                                <a href="{{ action('PagesController@post', $post->slug) }}">
                                    <img src="{{ $post->image }}" alt="{{ $post->title }}" class="img-fluid">
                                    <span>
                                        {{ $post->title }} <i class="fas fa-angle-double-right"></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <hr>
                <nav>
                    <ul class="pagination">
                        @if($blog->prev_page_url != null)
                            <li class="page-item">
                                <a class="page-link" href="?@if(isset($_GET['keyword']))keyword={{ $_GET['keyword'] }}&@endif{{ explode("?", $blog->prev_page_url)[1] }}" aria-label="Previous"><span aria-hidden="true">@lang('pagination.previous')</span></a>
                            </li>
                        @endif

                        @if($blog->next_page_url != null)
                            <li class="page-item">
                                <a class="page-link" href="?@if(isset($_GET['keyword']))keyword={{ $_GET['keyword'] }}&@endif{{ explode("?", $blog->next_page_url)[1] }}" aria-label="Previous"><span aria-hidden="true">@lang('pagination.next')</span></a>
                            </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    @endsection
