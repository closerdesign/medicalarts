@extends('layouts.app')

@section('content')

    <section id="title" v-scroll-reveal.reset>
        <h1>@lang('contact.title')</h1>
    </section>

    <section id="form" v-scroll-reveal.reset>
        <div class="container">
            @lang('contact.intro')
            <form action="{{ action('FormsController@contact') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="first_name">@lang('contact.first-name')</label>
                    <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required >
                </div>
                <div class="form-group">
                    <label for="last_name">@lang('contact.last-name')</label>
                    <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required >
                </div>
                <div class="form-group">
                    <label for="email">@lang('contact.email')</label>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" required >
                </div>
                <div class="form-group">
                    <label for="text">@lang('contact.phone')</label>
                    <input type="number" class="form-control" name="phone" value="{{ old('phone') }}" max="10" required >
                </div>
                <div class="form-group">
                    <label for="country">@lang('contact.country')</label>
                    <input type="text" class="form-control" name="country_name" value="{{ old('country_name') }}" required >
                </div>
                <div class="form-group">
                    <label for="comments">@lang('contact.comments')</label>
                    <textarea name="comments" id="comments" cols="30" rows="10" class="form-control">{{ old('comments') }}</textarea>
                </div>
                <button class="btn btn-primary btn-block">
                    @lang('contact.send')
                </button>
            </form>
        </div>
    </section>

    @endsection
