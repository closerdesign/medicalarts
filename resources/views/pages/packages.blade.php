@extends('layouts.app')

@section('content')

    <section id="title" v-scroll-reveal.reset>
        <h1>@lang('packages.title')</h1>
    </section>

    <section id="content" v-scroll-reveal.reset>
        <div class="container">

            <div class="row">

                @foreach( $packages[0]->products as $package )

                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">{{ $package->name }}</h5>
                                <p class="card-text">{!! $package->short_description !!}</p>
                                {!! $package->long_description !!}
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#ctaModal">@lang('general.assessment-request')</a>
                            </div>
                        </div>
                    </div>

                @endforeach

            </div>

        </div>
    </section>

    <section id="title" v-scroll-reveal.reset>
        <h1>@lang('packages.procedures')</h1>
    </section>

    <section id="content">
        <div class="container">

            <ul class="list-group">
                @foreach( $procedures[0]->products as $procedure )
                <li class="list-group-item">{{ $procedure->name }}</li>
                @endforeach
            </ul>

        </div>
    </section>

    <section id="title" v-scroll-reveal.reset>
        <h1>@lang('packages.additional-procedures')</h1>
    </section>

    <section id="content" v-scroll-reveal.reset>
        <div class="container">

            <ul class="list-group">
                @foreach( $additional_procedures[0]->products as $procedure )
                <li class="list-group-item">{{ $procedure->name }}</li>
                @endforeach
            </ul>

        </div>
    </section>

    <section id="title" v-scroll-reveal.reset>
        <h1>@lang('packages.other-procedures')</h1>
    </section>

    <section id="content" v-scroll-reveal.reset>
        <div class="container">

            <ul class="list-group">
                @foreach( $other_procedures[0]->products as $procedure )
                <li class="list-group-item">{{ $procedure->name }}</li>
                @endforeach
            </ul>

        </div>
    </section>

    @endsection
