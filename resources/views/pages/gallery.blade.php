@extends('layouts.app')

@section('content')

    <section id="title">
        <h1>@lang('general.gallery')</h1>
    </section>

    <div id="content">
        <div class="container-fluid">
            <div class="card-columns">

                @foreach($images as $image)
                <div class="card">
                    <a data-toggle="modal" data-target="#imageModal{{ $image->id }}" href="#">
                        <img class="card-img-top" src="{{ $image->image }}">
                    </a>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="imageModal{{ $image->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <img src="{{ $image->image }}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
                @endforeach

            </div>

        </div>
    </div>

    @endsection